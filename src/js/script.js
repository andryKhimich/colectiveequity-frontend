const humburger = $('.js-humburger');
const headerMenu = $('.js-menu');
const scrollBtn = $('.scroll-btn');
const header = $('.header');
const logoImg = $('.logo img');

function setInnerHeader() {
  logoImg.attr("src", logoBlackUrl);
  header.addClass('header_inner');
}

// init odometer numbers on scroll
function numbersInit() {
  $('.odometer').each(function () {
    var counter = $(this);
    var num = counter.attr('data-num');
    counter.html(num);
    if (counter.offset().top - $(window).scrollTop() < $(window).height() / 1.2) {
      counter.html(num);
    } else {
      return false
    }
  });
}


function setHomeHeader() {
  logoImg.attr("src", logoMainUrl);
  header.removeClass('header_inner');
}

function showOnScroll(scrollValue) {
  $('.js-scroll').each(function () {
    let elem = $(this);
    let sectionPos = elem.offset().top;
    let windowPos = $(window).scrollTop() + $(window).height() / 1.2;
    if (sectionPos < windowPos) {
      elem.removeClass('js-fadeIn js-slideLeft js-slideRight js-slideTop');
    }
  });

  // $('.js-active').each(function () {
  //   let item = $(this);
  //   let sectionPos = item.offset().top;
  //   let windowPos = $(window).scrollTop() + $(window).height() / 2.8;
  //   if (sectionPos < windowPos) {
  //     item.addClass('active');
  //   } else {
  //     item.removeClass('active');
  //   }
  // });
}

function openMenu() {
  humburger.addClass('open');
  headerMenu.addClass('open');
}

function closeMenu() {
  humburger.removeClass('open');
  headerMenu.removeClass('open');
}

function showContent() {
  $('.main-wrapper').removeClass('js-fadeIn');
}

$(document).ready(function () {
  // if ($('.inner-page').length > 0) {
  //   setInnerHeader();
  // } else {
  //   setHomeHeader();
  // }
  showContent();

  humburger.click(function () {
    if ($(this).hasClass('open')) {
      closeMenu();
    } else {
      openMenu();
    }
  });


  // slow scroll to id

  //   scrollBtn.click(function (e) {
  //     e.preventDefault();
  //     let link = $($(this).attr('href'))
  //     $('html, body').animate({
  //       scrollTop: link.offset().top
  //     }, 1000);
  //   });

  showOnScroll($(window).scrollTop());

  $(window).scroll(function () {
    const scrollValue = $(this).scrollTop();
    showOnScroll(scrollValue);
    scrollValue >= 1 ? closeMenu() : null;

    // if (scrollValue > 1) {
    //   header.addClass('sticky');
    // } else {
    //   header.removeClass('sticky');
    //   // logoImg.attr("src", logoColorUrl);
    // }
    numbersInit();
  });

  $('.past-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    infinite: false,
    prevArrow: $('.js-past-prev'),
    nextArrow: $('.js-past-next'),
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false
      }
    }]
  });

  $('.slider__container').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    fade: true,
    infinite: false,
    prevArrow: $('.js-slider-prev'),
    nextArrow: $('.js-slider-next')
  });
  // $('.testimonials-slider__wrapper').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   autoplay: true,
  //   dots: true,
  //   arrows: false,
  //   infinite: true,
  //   fade: true,
  //   speed: 1000,
  //   cssEase: 'linear',
  //   autoplaySpeed: 10000,
  //   arrows: true,
  //   prevArrow: $('.testimonials-slider_prev'),
  //   nextArrow: $('.testimonials-slider_next')
  // });
  google.maps.event.addDomListener(window, 'load', init);

  function init() {
    var mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(40.607834, -73.913577),
      disableDefaultUI: true,
      styles: [{
          "featureType": "all",
          "elementType": "all",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "administrative",
          "elementType": "labels.text.fill",
          "stylers": [{
            "color": "#444444"
          }]
        },
        {
          "featureType": "administrative.province",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.neighborhood",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [{
            "color": "#f2f2f2"
          }]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "all",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [{
              "visibility": "off"
            },
            {
              "color": "#cee9de"
            },
            {
              "saturation": "2"
            },
            {
              "weight": "0.80"
            }
          ]
        },
        {
          "featureType": "poi.attraction",
          "elementType": "geometry.fill",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "poi.park",
          "elementType": "all",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [{
              "saturation": -100
            },
            {
              "lightness": 45
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "all",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [{
              "visibility": "on"
            },
            {
              "color": "#f5d6d6"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.icon",
          "stylers": [{
              "hue": "#ff0000"
            },
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "road.highway.controlled_access",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "road.highway.controlled_access",
          "elementType": "labels.icon",
          "stylers": [{
              "visibility": "on"
            },
            {
              "hue": "#0064ff"
            },
            {
              "gamma": "1.44"
            },
            {
              "lightness": "-3"
            },
            {
              "weight": "1.69"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "all",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "road.local",
          "elementType": "all",
          "stylers": [{
            "visibility": "on"
          }]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text",
          "stylers": [{
              "visibility": "simplified"
            },
            {
              "weight": "0.31"
            },
            {
              "gamma": "1.43"
            },
            {
              "lightness": "-5"
            },
            {
              "saturation": "-22"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [{
            "visibility": "off"
          }]
        },
        {
          "featureType": "transit.line",
          "elementType": "all",
          "stylers": [{
              "visibility": "on"
            },
            {
              "hue": "#ff0000"
            }
          ]
        },
        {
          "featureType": "transit.station.airport",
          "elementType": "all",
          "stylers": [{
              "visibility": "simplified"
            },
            {
              "hue": "#ff0045"
            }
          ]
        },
        {
          "featureType": "transit.station.bus",
          "elementType": "all",
          "stylers": [{
              "visibility": "on"
            },
            {
              "hue": "#00d1ff"
            }
          ]
        },
        {
          "featureType": "transit.station.bus",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "transit.station.rail",
          "elementType": "all",
          "stylers": [{
              "visibility": "simplified"
            },
            {
              "hue": "#00cbff"
            }
          ]
        },
        {
          "featureType": "transit.station.rail",
          "elementType": "labels.text",
          "stylers": [{
            "visibility": "simplified"
          }]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [{
              "color": "#46bcec"
            },
            {
              "visibility": "on"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [{
              "weight": "1.61"
            },
            {
              "color": "#cde2e5"
            },
            {
              "visibility": "on"
            }
          ]
        }
      ]
    };
    var mapElement = document.getElementById('map')
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(40.607834, -73.913577),
      map: map,
      icon: '../img/svg/pin.svg'
    });
  }
});
svg4everybody();


console.log('hello');